'use strict'

/** Real Credentials SMS PRO */
const accountSid_sms = 'AC5c59e07fcd80c7347c98423cae23fa55'
const authToken_sms = '81affa192f89404cc7f9f81d19cab868'
const client_sms = require('twilio')(accountSid_sms, authToken_sms) 

/** Real Credentials SMS */
// const accountSid_sms = 'AC205f1c7d661c115b70d7bf229f94e9b5'
// const authToken_sms = '7f5d2bd1a7fe1357f1c95464dfe4f0cb'
// const client_sms = require('twilio')(accountSid_sms, authToken_sms) 

/** Real Credentials Calling */
const accountSid_call = 'ACe1c29858014a50e383805c4d58ddce83'
const authToken_call = '9012413736a3139f216db74f696e6899'
const client_call = require('twilio')(accountSid_call, authToken_call) 

/** Real Credentials WhatsApp */
const accountSid_wsp = 'AC5824b7b8dacba5a7ddad92efe9bbd00b'
const authToken_wsp = '2177b36dc3654a797c3f91842200641e'
const client_wsp = require('twilio')(accountSid_wsp, authToken_wsp) 

class SendbytwilioController {/** Constructor */

    async sms({ view }) { 
        console.log('En welcome: sms')
        client_sms.messages
            .create({
            body: 'Este es un mensaje de prueba de ASUcuenta. Psdt: Si recibes este mensaje avisanos en el grupo de Slack xd',
            to: '+51986314123',
            from: '+15162724900'
            })
            .then(message => console.log(message.sid))
            .done()
        return view.render('welcome', {msg: "SMS Enviado"})
    }

    async call({ view }) { 
        console.log('En welcome: call')
        client_call.calls
          .create({
             url: 'http://demo.twilio.com/docs/voice.xml',
             to: '+51982130528',
             from: '+5117009038'
            })
            .then(message => console.log(message.sid))
            .done()
        return view.render('welcome', {msg: "Llamada en camino"})
    }

    async wsp({ view }) {
        console.log('En welcome: wsp')
        client_wsp.messages
            .create({
                body: 'Hola mundo.. mandado desde nodejs',
                from: 'whatsapp:+14155238886',
                to: 'whatsapp:+51982130528'
            })
            .then(message => console.log(message.sid))
            .done()
        return view.render('welcome', {msg: "Mensaje de WhatsApp enviado"})
    }
    
}

module.exports = SendbytwilioController
